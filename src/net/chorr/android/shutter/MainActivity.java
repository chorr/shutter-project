package net.chorr.android.shutter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceView;
import android.widget.Toast;

public class MainActivity extends Activity {

	public static final int MEDIA_TYPE_IMAGE = 1;
	private Camera mCamera;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mCamera = getCameraInstance();

		Parameters params = mCamera.getParameters();
		params.setPictureFormat(ImageFormat.JPEG);
		mCamera.setParameters(params);

		SurfaceView sv = new SurfaceView(getBaseContext());
		try {
			mCamera.setPreviewDisplay(sv.getHolder());
			mCamera.startPreview();
			mCamera.takePicture(null, null, mPicture);
		} catch (Exception e) {
			Log.e("EX", e.getMessage());
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		releaseCamera();
	}

	private void releaseCamera() {
		if (mCamera != null) {
			mCamera.release();
			mCamera = null;
		}
	}

	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open();
		} catch (Exception e) {
			Log.e("EX", e.getMessage());
			e.printStackTrace();
		}
		return c;
	}

	private PictureCallback mPicture = new PictureCallback() {
		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			Log.i("EX", "onPictureTaken()");
//			AudioManager mgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//			mgr.setStreamMute(AudioManager.STREAM_SYSTEM, true);
//			mgr.setStreamVolume(AudioManager.STREAM_SYSTEM, 0,
//					AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);

			File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
			if (pictureFile == null) {
				Log.d("EX",
						"Error creating media file, check storage permissions");
				return;
			}

			try {
				FileOutputStream fos = new FileOutputStream(pictureFile);
				fos.write(data);
				fos.close();
			} catch (FileNotFoundException e) {
				Log.d("EX", "File not found: " + e.getMessage());
			} catch (IOException e) {
				Log.d("EX", "Error accessing file: " + e.getMessage());
			}

			finish();
		}
	};

	private File getOutputMediaFile(int type) {
		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				getString(R.string.app_name));

		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("EX", "failed to create directory");
			}
		}

		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());

		File mediaFile;
		Log.i("EX", "# ���� : " + mediaStorageDir.getPath() + File.separator
				+ "IMG_" + timeStamp + ".jpg");
		Toast.makeText(
				this,
				mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp
						+ ".jpg", Toast.LENGTH_SHORT).show();

		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
		} else {
			return null;
		}

		return mediaFile;
	}

}
